class SimulatorsController < ApplicationController

  def new
    @simulator = Simulador.new
  end


  def calcularMeses(edad, sexo)
    if sexo=='Masculino'
      return (65-edad)
    else
      return (60-edad)
    end
  end

  def APO(edad, sexo, apo, sueldo) #APO final
    tiempo = calcularMeses(edad, sexo)
    for i in 1..tiempo
      apo = apo + ((sueldo*0.1)*12)
      apo = apo*(1.05)
    end
    return apo
  end

  def APV(edad, sexo, sueldo, apo, pensionDeseada) #APV necesario para monto deseado
    if sexo == 'Masculino'
      return pensionDeseada*240 - APO(edad, sexo, apo, sueldo)
    else
      return pensionDeseada*360 - APO(edad, sexo, apo, sueldo)
    end
  end

  def APV_mensual(edad, sexo, sueldo, apo, pensionDeseada) #APV mensual para monto deseado
    tiempo = calcularMeses(edad,sexo)
    apvAcumulado = APV(edad, sexo, sueldo, apo, pensionDeseada)
    for i in 1...tiempo
      apvAcumulado = apvAcumulado - (apvAcumulado*0.15)
    end
    return (apvAcumulado/12).abs
  end

  def APV_final(edad, sexo, apvAcumulado, apvMensual) #APV final dado un fondo (puede ser 0) y una cantidad de dinero mensual
    tiempo = calcularMeses(edad, sexo)
    for i in 1..tiempo
      apvAcumulado = apvAcumulado + (apvMensual*12)
      apvAcumulado = apvAcumulado*1.05
    end
    return apvAcumulado
  end

  def pension_mensual(fondo, sexo)
    if sexo=='Masculino'
      periodo = 20*12
      fondo/periodo
    else
      periodo = 30*12
      fondo/periodo
    end
  end

  def resultadoAPO
    @simulacion = Simulador.find(params[:id])
    @simulaciones = Array.new(2)
    @simulaciones[0] = APO(@simulacion.edad,@simulacion.sexo,@simulacion.apo,@simulacion.sueldo) + APV_final(@simulacion.edad,@simulacion.sexo, @simulacion.apvAcumulado, @simulacion.apvMensual)
    @simulaciones[1] = pension_mensual(@simulaciones[0], @simulacion.sexo)
  end

  def resultadoAPV
    @simulacion = Simulador.find(params[:id])
    @simulaciones = Array.new(1)
    @simulaciones[0] = @simulacion.pensionDeseada
    @simulaciones[1] = APV_mensual(@simulacion.edad, @simulacion.sexo, @simulacion.sueldo, APO(@simulacion.edad,@simulacion.sexo,@simulacion.apo,@simulacion.sueldo), @simulacion.pensionDeseada )


  end

  def create
    @simulacion = Simulador.new(simulador_params)
    if @simulacion.pensionDeseada.nil?
      @simulacion.pensionDeseada = 0
      @simulacion.save
      redirect_to :action => 'resultadoAPO', :id => @simulacion.id
    else
      @simulacion.apvMensual=0
      @simulacion.apvAcumulado=0
      @simulacion.save
      redirect_to :action => 'resultadoAPV', :id => @simulacion.id
    end
  end

  private
  def simulador_params
    params.require(:simulador).permit(:nombre, :edad, :sexo, :apo, :sueldo, :apvMensual, :apvAcumulado, :pensionDeseada)
  end
end
