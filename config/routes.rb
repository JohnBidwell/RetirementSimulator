Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'simulators/index'
  get 'simulators/apv', as: 'apv'
  post "simulators/apv" => "simulators#create"
  get 'simulators/apo', as: 'apo'
  post "simulators/apo" => "simulators#create"
  get 'simulators/resultadoAPO' => 'resultadoAPO', as: 'resultadoAPO'
  get 'simulators/resultadoAPV' => 'resultadoAPV', as: 'resultadoAPV'


  #get 'simulators/resultado'
  root to: 'simulators#index'
end
