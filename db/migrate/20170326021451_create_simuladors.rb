class CreateSimuladors < ActiveRecord::Migration[5.0]
  def change
    create_table :simuladors do |t|
      t.string :nombre
      t.integer :edad
      t.string :sexo
      t.integer :apo
      t.integer :sueldo
      t.integer :apvMensual
      t.integer :apvAcumulado
      t.integer :pensionDeseada

      t.timestamps
    end
  end
end
